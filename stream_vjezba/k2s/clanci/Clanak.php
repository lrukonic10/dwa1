<?php

/*
Napravite datoteku “Clanak.php”:
a.  u njoj deklarirajte klasu “Clanak”.
b. Prema strukturi tablice “članci” u BP, odredite svojstva klase.
c. Napravite konstruktorsku funkciju koja prima id zapisa iz baze, dohvaća 
podatke iz BP I postavlja attribute klase na dohvacene vrijednosti.
d. Napravite metode Objavi() i Sakrij() koje ažuriraju članak u bazi mijenjajući 
vrijednost atributa “objavljen” iz 0 u 1 i obrnuto.
e. Metoda “Link_Na_Clanak($id) ispisuje  naslov jednog članka u obliku linka koji 
vodi na prikaz cjelovitog članka (link = zadatak2.php) Koristiti će se za ispis
popisa svih članaka u bazi. 
f. Metoda “Prikaz($id)” koristiti za prikaz cjelovitog odabranog članka.
*/


class Clanak{

    public $id;
    public $naslov;
    public $sadrzaj;
    public $autor;
    public $datum;
    public $jeObjavljeno;
    
    public function Clanak($id=""){
        if($id!=""){
            $baza = $this->citaj();
            foreach($baza as $clanak){
                if($clanak[0] == $id){
                    $this->id = $clanak[0];
                    $this->naslov = $clanak[1];
                    $this->sadrzaj = $clanak[2];
                    $this->autor = $clanak[3];
                    $this->datum = $clanak[4];
                    $this->jeObjavljeno = $clanak[5];
                    return;
                }
            }
        }
    }
    
    public function objavi(){
        $baza = $this->citaj();
        $novaBaza = array();
        foreach($baza as $clanak){
            if($clanak[0] == $this->id){
                $clanak[5] = 1;
            }
            $novaBaza[] = $clanak;
        }
        $this->pisi($novaBaza);
    }
    
    public function sakrij(){
        $baza = $this->citaj();
        $novaBaza = array();
        foreach($baza as $clanak){
            if($clanak[0] == $this->id){
                $clanak[5] = 0;
            }
            $novaBaza[] = $clanak;
        }
        $this->pisi($novaBaza);
    }
    
    public static function linkNaClanak($id){
        $baza = Clanak::citaj();
        foreach($baza as $clanak){
            if($clanak[0] == $id){
                echo "<a href=\"z2.php?id=".$clanak[0]."\">". $clanak[1] ."</a><br />";
                return;
            }
        }
    }
    
    public static function prikaz($id){
        $baza = Clanak::citaj();
        foreach($baza as $clanak){
            if($clanak[0] == $id){
                ?>

                <table>
                    <tr><td>ID</td><td><?=$clanak[0]?></td></tr>
                    <tr><td>Naslov</td><td><?=$clanak[1]?></td></tr>
                    <tr><td>Sadržaj</td><td><?=$clanak[2]?></td></tr>
                    <tr><td>Autor</td><td><?=$clanak[3]?></td></tr>
                    <tr><td>Datum</td><td><?=$clanak[4]?></td></tr>
                    <tr><td>Objavljeno?</td><td><?=$clanak[5]?></td></tr>
                </table>

                <?php
                return;
            }
        }
    }


    public function citaj(){
        $baza = array();
        $dat = fopen("clanci.txt", "r");
        while(!feof($dat)){
            $baza[] = explode("-",trim(fgets($dat)));
        }
        array_pop($baza);
        fclose($dat);
        return $baza;
    }
    
    public function pisi($baza){
        $dat = fopen("clanci.txt", "w");
        foreach($baza as $clanak){
            $string = implode("-",$clanak) . "\n";
            fwrite($dat,$string);
        }
        fclose($dat);
    }
    
    private function printer($a){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }
}

?>