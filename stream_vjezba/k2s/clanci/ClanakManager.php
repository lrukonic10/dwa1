<?php
/*
 2. Napravite datoteku “ClanakManager.php”
    a.  u njoj deklarirajte statičnu metodu PrikazSvihClanaka() koja dohvaca id-eve 
        svih clanaka u bazi i zatim instancira jedan po jedan članak, te ga prikazuje na
        ekranu pozivom metode iz zadatka 1e.
    b.  deklarirajte i metodu UnosNovog() koja prikazuje obrazac za unos novog
        članka.
    c.  deklarirajte i metodu SpremiClanak() koja prikuplja podatke sa obrasca i
        upisuje novi zapis u BP.
 */

require_once "Clanak.php";

class ClanakManager{
    public static function prikazSvihClanaka(){
        $ids = array();
        $baza = Clanak::citaj();
        foreach($baza as $clanak){
            $ids[] = $clanak[0];
        }
        foreach($ids as $id){
            // $clanak = new Clanak($id);
            Clanak::linkNaClanak($id);
        }
    }
    
    public static function UnosNovog(){
        ?>
            <form method="post">
                <table>
                    <tr>
                        <td>ID</td>
                        <td><input type="text" name="id" /></td>
                    </tr>
                    <tr>
                        <td>Naslov</td>
                        <td><input type="text" name="naslov" /></td>
                    </tr>
                    <tr>
                        <td>Sadržaj</td>
                        <td><input type="text" name="sadrzaj" /></td>
                    </tr>
                    <tr>
                        <td>Autor</td>
                        <td><input type="text" name="autor" /></td>
                    </tr>
                    <tr>
                        <td>Datum</td>
                        <td><input type="text" name="datum" /></td>
                    </tr>
                    <tr>
                        <td>Objavljeno (0|1)</td>
                        <td><input type="text" name="objavljeno" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" value="Unos" /></td>
                    </tr>
                </table>
            </form>
        <?php
    }
    
    public static function spremiClanak(){
        $zaUpis = array($_POST["id"], $_POST["naslov"], $_POST["sadrzaj"], $_POST["autor"], $_POST["datum"], $_POST["objavljeno"]);
        $baza = Clanak::citaj();
        $baza[] = $zaUpis;
        Clanak::pisi($baza);
    }
}

?>