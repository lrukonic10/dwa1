<?php

    function ispisPolja($a){
        echo "<pre>";
        print_r ($a);
        echo "</pre>";
    }

    if(empty($_POST["username"]) || empty($_POST["password"]))
        die("Username i/ili password su prazni. Pokusajte ponovno. <br /> <a href=\"login.html\">Nazad</a>");
    
    $user = @$_POST["username"];
    $password = @$_POST["password"];
    
    // Čitanje iz datoteke
    
    $dat = fopen("popis.txt", "r");
    
    $s = "";
    
    /*
     * baza{
     *  [0]{
     *      [0] username
     *      [1] pass
     *     }
     *  [1]{
     *      [0] username
     *      [1] pass
     *     }
     *
     * }
     */
    
    $podaci = array();
    
    while (!feof($dat)){
       array_push ($podaci, fgets($dat));
    }
    
    $baza = array();
    
    foreach ($podaci as $redak){
       array_push ($baza, explode("\t",trim($redak)));
    }
    
    //ispisPolja($podaci);
    //ispisPolja($baza);
    
    // PROVJERA PODATAKA
    
    $prijavljen = false;
    foreach($baza as $korisnik){
        if ($user == $korisnik[0] && $password == $korisnik[1]){
            // Login
            $prijavljen = true;
            break;
        }
    }
    
    if ($prijavljen){
        echo "DOBRODOŠAO " . $user;
    }else{
		die("Neispravni podaci. Pokusajte ponovno.");
    }
    
    fclose($dat);
    
?>

<a href="login.html">Nazad</a>


<?php

    // OBRAZAC

    if ($prijavljen){
    ?>
    
        <form action="obrada.php" method="POST">
            <table cellspacing="5" cellpadding="5" border="0">
                <tr>
                    <td>Odabari proizvod</td>
                    <td>
                        <select name="proizvod">
                            <option>T-shirt 100 kn</option>
                            <option>Trenerka 200 kn</option>
                            <option>Traperice 300 kn</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Odaberi veličinu</td>
                    <td>
                        <select name="velicina">
                            <option>LARGE 100 kn</option>
                            <option>EXTRALARGE 200 kn</option>
                            <option>XXL 300 kn</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <input type='hidden' name='user' value='<?=$user?>' />
                    <td><input type="submit" value="Naruči"/></td>
                </tr>
            </table>
        </form>
        
    <?php
    }

?>