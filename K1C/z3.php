<?php

    function ispisNiza($a){
	echo "<pre>";
	print_r($a);
	echo "</pre>";
    }
    
    //ispisNiza($_POST);
    

    $baza = array();
    
    $dat = fopen("countries.txt", "r");
    
    while(!feof($dat)){
	array_push($baza, explode("\t", trim(fgets($dat))));
    }
    array_pop($baza); // brisem zadnji row uzrokovan zadnjim \n
    fclose($dat);
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
	
	<h1 align="center">3. Zadatak</h1>
	
	<div align="center">
	    <form method="POST">
		<table cellspacing="5" cellpadding="5" border="0">
		    <tr>
			<td>Odaberite regiju</td>
			<td>
			    <select name="regija">
				
				<?php
				    $regije = array();
				    foreach($baza as $drzava){
					$postoji = false;
					foreach($regije as $regija){
					    if ($drzava[3] == $regija){
						$postoji = true;
						break;
					    }
					}
					if($postoji) continue;
					array_push($regije,$drzava[3]);
				?>
				
				<option><?=$drzava[3]?></option>
				
				<?php
				    }
				?>
				
			    </select>
			</td>
		    </tr>
		    <tr>
			<td>&nbsp;</td>
			<td><input type="submit" value="Pošalji"/></td>
		    </tr>
		</table>
	    </form>
	    
	    <br /><br /><hr /><br /><br />
	    
	    <?php
		if(isset($_POST['regija'])){
		    $regija = $_POST['regija'];
		    
		    $nova_baza = array();
		    foreach($baza as $drzava){
			if($drzava[3] == $regija) array_push($nova_baza, $drzava);
		    }
		    
		
	    ?>

	    <table cellspacing="1" cellpadding="5" border="1">
		<tr>
		    <th>NAME</th>
		    <th>CONTINENT</th>
		    <th>REGION</th>
		    <th>LIFE_EXPECTANCY</th>
		</tr>
		
		<?php
		
		    foreach($nova_baza as $z){
		?>
		    
		<tr>
		    <td><?=$z[1]?></td>
		    <td><?=$z[2]?></td>
		    <td><?=$z[3]?></td>
		    <td><?=$z[7]?></td>
		</tr>
		
		<?php	
		    }
		}
		?>
		
	    </table>
	    
	</div>
    </body>
</html>
