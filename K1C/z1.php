<?php

    function ispisNiza($a){
	echo "<pre>";
	print_r($a);
	echo "</pre>";
    }
    
    //ispisNiza($_POST);
    

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
	
	<h1 align="center">1. Zadatak</h1>
	
	<div align="center">
	    
	    <form method="POST">
		
		<table cellspacing="5" cellpadding="5" border="0">
		    <tr>
			<td>1. broj</td>
			<td><input type="text" name="prvi" /></td>
		    </tr>
		    <tr>
			<td>2. broj</td>
			<td><input type="text" name="drugi" /></td>
		    </tr>
		    <tr>
			<td>Operacije</td>
			<td>
			    +<input type="checkbox" name="operacija[]" value="zbr" />&nbsp;&nbsp;|&nbsp;&nbsp;
			    -<input type="checkbox" name="operacija[]" value="odu" />&nbsp;&nbsp;|&nbsp;&nbsp;
			    *<input type="checkbox" name="operacija[]" value="mno" />&nbsp;&nbsp;|&nbsp;&nbsp;
			    /<input type="checkbox" name="operacija[]" value="dje" />
			</td>
		    </tr>
		    <tr>
			<td>&nbsp;</td>
			<td><input type="submit" value="Pošalji"/></td>
		    </tr>
		</table>
		
		<br /><br /><hr /><br /><br />
		
		<?php
		    if(isset($_POST['prvi']) && isset($_POST['drugi']) && isset($_POST['operacija'])){
			
			$prvi = $_POST['prvi'];
			$drugi= $_POST['drugi'];
			$oper = $_POST['operacija'];
			
			echo "Prvi broj: ". $prvi . "<br />";
			echo "Drugi broj: ". $drugi . "<br /><br />";


			foreach ($oper as $o){
			    switch ($o){
				case 'zbr':
				    echo "Rezultat zbrajanja je: " . ($prvi + $drugi) . "<br />";
				    break;
				case 'odu':
				    echo "Rezultat oduzimanja je: " . ($prvi - $drugi) . "<br />";
				    break;
				case 'mno':
				    echo "Rezultat množenja je: " . ($prvi * $drugi) . "<br />";
				    break;
				case 'dje':
				    echo "Rezultat dijeljenja je: " . ($prvi / $drugi) . "<br />";
				    break;
			    }
			}
			
		    }
		?>
		
	    </form>
	    
	</div>
	
    </body>
</html>
