<?php

    function ispisNiza($a){
	echo "<pre>";
	print_r($a);
	echo "</pre>";
    }
    
    //ispisNiza($_POST);
    
    $baza = array();
    
    $dat = fopen("countries.txt", "r");
    
    while(!feof($dat)){
	$row = explode("\t", trim(fgets($dat)));
	//ispisNiza($row);
	$row[8] = (isset($row[8]))?$row[8]:0;
	$row[9] = (isset($row[9]))?$row[9]:0;
	$row[9] = ($row[8] > $row[9])?"RAST":"PAD";
	array_push($baza, $row);
    }
    array_pop($baza); // brisem zadnji row uzrokovan zadnjim \n
    fclose($dat);
    
    //ispisNiza($baza);
    
    
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
	
	<h1 align="center">2. Zadatak</h1>
	
	<div align="center">
	    
	    <table cellspacing="1" cellpadding="5" border="1">
		<tr>
		    <th>NAME</th>
		    <th>SURFACE AREA</th>
		    <th>LIFE EXPECTANCY</th>
		    <th>GNP</th>
		    <th>PRIRAST GNPa</th>
		</tr>
		
		<?php
		
		    $br = 0;
		    foreach($baza as $zemlja){
			$br++;
			if($br < 15 || $br > 24) continue;
		?>
		    
		<tr>
		    <td><?=$zemlja[1]?></td>
		    <td><?=$zemlja[4]?></td>
		    <td><?=$zemlja[7]?></td>
		    <td><?=$zemlja[8]?></td>
		    <td><?=$zemlja[9]?></td>
		</tr>
		
		<?php	
		    }
		
		?>
		
	    </table>
	 
	</div>
	
    </body>
</html>
