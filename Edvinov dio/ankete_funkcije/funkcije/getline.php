<?php

function getline($path, $id, $value) {
    if (file_exists($path)) {// DA LI POSTOJI DATOTEKA?
        if (is_readable($path)) {// MOGU LI JE JA ČITATI?
            if (filesize($path) > 0) {// JE LI PRAZAN?

                $fh = fopen($path, 'r');
                while (($red = fgets($fh, 4096)) !== false) {
                    $redak = explode("\t", $red);
                    if ($redak[$id] == $value) {
                        fclose($fh);
                        return $redak;
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

?>