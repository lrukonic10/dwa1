<?php

function del($path, $id, $value) {
    if (file_exists($path)) {
        if (is_writeable($path)) {

            $fh = fopen($path, 'r');
            $sve = array();
            while (($red = fgets($fh, 4096)) !== false) {
                $redak = explode("\t", trim($red));
                if ($redak[$id] != $value) { // SVI RETCI OSIM ONOG KOJEG BRISEMO
                    $red = implode("\t", $redak);
                    $sve[] = trim($red);
                }
            }
            fclose($fh);

            $sadrzaj = implode("\n", $sve);
            $sadrzaj = $sadrzaj . "\n";
            // SPREMI
            save($path, $sadrzaj, 'w');
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

?>