<?php

function preselect($path,$id,$value,$fields,$name)
{
    if(file_exists($path))// DA LI POSTOJI DATOTEKA?
	{
		if(is_readable($path))// MOGU LI JE JA ČITATI?
		{
			if(filesize($path)>0)// JE LI PRAZAN?
			{

				// CITANJE IZ TXT DATOTEKE
				$fh = fopen($path,'r');
                                $rez = '';
                                $rez.='<select name="'.$name.'">';
				while (($red = fgets($fh, 4096)) !== false) 
				{	
                                    $redak = explode("\t",$red);
                                    $rez.='<option value="'.$redak[$id];
                                    if($redak[$id]==$value){ $rez.=' selected '; }
                                    $rez.='">';
                                    if(is_array($fields)){
                                        foreach ($fields as $f) {
                                            $rez.= $redak[$f].' ';
                                        }
                                    } else { $rez.= $redak[$fields]; }
                                    $rez.='</option>';
				}
                                $rez.='</select>';
				fclose($fh); 
                                return $rez;
			} 
                        else { return false; }
		} else { return false; }
	} else { return false; }

}
?>