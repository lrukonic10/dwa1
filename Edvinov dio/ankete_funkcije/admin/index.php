﻿<html>
    <head>
        <title>Ankete :: Admin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <?php
        $path = '/home/ijugo/public_html/dwa1/ankete.txt';
        require 'funkcije/global.php';

        if (isset($_GET['a'])) {
            $a = $_GET['a'];
        } else {
            $a = '';
        }
        switch ($a) {
            case 'insert' : insert();
                break; // UNOS NOVE ANKETE
            case 'editform': editForm();
                break; // OBRAZAC ZA IZMJENU
            case 'editsave': editSave();
                break; // SPREMI IZMJENE
            case 'confirm' : confirm();
                break; // JESTE LI SIGURNI
            case 'delete' : del();
                break; // BRISANJE
            default: pregled();
                form(); // SVE ANKETE I OBRAZAC ZA NOVU
        }

        /* ********************* */
        /* 			 */
        /*    UNOS NOVE ANKETE	 */
        /* 			 */
        /* ********************* */

        function insert() {
            global $path;
            $rbr = count(file($path)) + 1;
            $pitanje = $_POST['pitanje'];
            $odgovor1 = $_POST['o1'];
            $odgovor2 = $_POST['o2'];
            $odgovor3 = $_POST['o3'];
            $odgovor4 = $_POST['o4'];
            $br = 0;

            // PRIPREMA RETKA ZA UPIS
            define('SEP', "\t");
            $redak = $rbr . SEP . $pitanje . SEP . $odgovor1 . SEP . $odgovor2 .
                    SEP . $odgovor3 . SEP . $odgovor4 . SEP . $br . SEP . $br . SEP . $br . SEP . $br . "\n";

            // UNESI
            save($path, $sadrzaj, 'a');

            echo '<h1>Uspjesno uneseno</h1>';
            echo link($_SERVER['SCRIPT_NAME'], array(), 'Povratak na pregled');
        }

        /********************** */
        /* 			 */
        /* OBRAZAC ZA IZMJENU	 */
        /* 			 */
        /********************** */

        function editForm() {
            // DOHVATI ID ANKETE KOJU MIJENJAMO
            global $path;
            $id = $_GET['id'];
            // DOHVATI REDAK SA PODACIMA
            $redak = getline($path, 0, $id);

            // PRIKAZI OBRAZAC ISTI KAO KOD UNOSA ALI POPUNJEN PODACIMA
            echo '<form name="form1" method="post" action="?a=editsave">
  		<fieldset><legend>Unos nove ankete</legend>&nbsp;</p>
  		<p>Pitanje: 
    	<input name="pitanje" type="text" id="pitanje" size="60" 
			value="' . $redak[1] . '">
  		</p>
  		<p>Odgovor 1: 
    		<input name="o1" type="text" id="o1" value="' . $redak[2] . '">
  		</p>
  		<p>Odgovor 2: 
    		<input name="o2" type="text" id="o2" value="' . $redak[3] . '">
  		</p>
  		<p>Odgovor 3: 
    		<input name="o3" type="text" id="o3" value="' . $redak[4] . '">
  		</p>
  		<p>Odgovor 4: 
    		<input name="o4" type="text" id="o4" value="' . $redak[5] . '">
  		</p>
  		<p>
    		<input type="submit" name="Submit" value="Submit">
			<input type="hidden" name="id" value="' . $redak[0] . '">
  		</p>
  	</fieldset>
	</form>';
        }

        /********************** */
        /* 			 */
        /* SPREMI IZMJENE  	 */
        /*              	 */
        /********************** */

        function editSave() {
            global $path;
            $id = $_POST['id'];
            $odgovor = $_POST['anketa'];

            $fh = fopen($path, 'r'); // OTVORI FILE ZA CITANJE
            $sve = array();   // POLJE ZA PRIJENOS SADRZAJA
            while (($red = fgets($fh, 4096)) !== false) {
                $redak = explode("\t", $red);
                if ($redak[0] == $id) { // NASLI SMO TRAZENU ANKETU
                    $redak[1] = $_POST['pitanje'];
                    $redak[2] = $_POST['o1'];
                    $redak[3] = $_POST['o2'];
                    $redak[4] = $_POST['o3'];
                    $redak[5] = $_POST['o4'];
                    $red = implode("\t", $redak); // VRATI U STRING
                }
                $sve[] = trim($red);  // DODAJ STRING U POLJE ZA PRIJENOS
            }
            fclose($fh);

            $sadrzaj = implode("\n", $sve); // PRETVORI U VELIKI STRING

            $sadrzaj = $sadrzaj . "\n";

            // UNESI
            save($path, $sadrzaj, 'w');

            echo '<h1>Spremljeno!</h1>';
            echo '<p>' . link($_SERVER['SCRIPT_NAME'], array('a' => 'prikaz', 'id' => $id), 'Povratak na anketu') . '</p>';
            echo '<p>' . link($_SERVER['SCRIPT_NAME'], array(), 'Povratak na pregled anketa') . '</p>';
        }

        /********************** */
        /* 			 */
        /* POTVRDA BRISANJA	 */
        /* 			 */
        /********************* */

        function confirm() {
            global $path;
            $id = $_GET['id']; // KOJU ANKETU UZETI IZ DATOTEKE
            // DOHVATI REDAK SA PODACIMA
            $redak = getline($path, 0, $id);

            echo '<h1>' . $redak[1] . '</h1>';
            echo '<h2>Jeste li sigurni da zelite izbrisati anketu</h2>';
            echo '<form method="post" action="?a=delete">';
            echo '<input type="radio" name="odg" value="1">DA 
	  			  	<input type="radio" name="odg" value="0">NE';
            echo '<input type="submit" name="Submit" value="Potvrdi">';
            echo '<input type="hidden" name="id" value="' . $redak[0] . '">';
            echo '</form>';
            // NASLI SMO ANKETU, PREKINI WHILE PETLJU
           

            fclose($fh);
        }

        /********************** */
        /* 			 */
        /* KONACNO BRISANJE      */
        /* 			 */
        /********************** */

        function del() {
            global $path;
            $id = $_POST['id'];
            $odgovor = $_POST['odg'];
            // GLAVNI IF 
            if ($odgovor == 1) { // ODGOVOR JE BIO "DA"
                // IZBRISI REDAK
                del($path, 0, $id);


                echo '<h1>Anketa je izbrisana!</h1>';
                echo '<p>' . link($_SERVER['SCRIPT_NAME'], array(), 'Povratak na pregled') . '</p>';
            } else {
                echo '<p>' . link($_SERVER['SCRIPT_NAME'], array('a' => 'prikaz', 'id' => $id), 'Povratak na anketu') . '</p>';
            }
        }

        /********************** */
        /* 			 */
        /* PREGLED SVIH ANKETA	 */
        /*              	 */
        /********************** */

        function pregled() {
            global $path;

            if (file_exists($path)) {// DA LI POSTOJI DATOTEKA?
                if (is_readable($path)) {// MOGU LI JE JA ČITATI?
                    if (filesize($path) > 0) {// JE LI PRAZAN?

                        // CITANJE IZ TXT DATOTEKE
                        $fh = fopen($path, 'r');
                        echo '<table border="1" cellpadding="4">';
                        while (($red = fgets($fh, 4096)) !== false) {
                            $redak = explode("\t", $red);
                            echo '<tr>';
                            // PRVI STUPAC = ANKETNO PITANJE
                            echo '<td>' . $redak[1] . '</td>';
                            // DRUGI STUPAC = LINK ZA IZMJENU
                            echo '<td>';
                            echo link($_SERVER['SCRIPT_NAME'], array('a' => 'editform', 'id' => $redak[0]), 'EDIT');
                            echo '</td>';
                            // TRECI STUPAC = LINK ZA BRISANJE
                            echo '<td>';
                            echo link($_SERVER['SCRIPT_NAME'], array('a' => 'confirm', 'id' => $redak[0]), 'DELETE');
                            echo '</td>';
                            echo '</tr>';
                        }
                        echo '</table>';
                        fclose($fh);
                    }
                }
            }
        }

        /********************** */
        /* 			 */
        /* OBRAZAC ZA NOVU AN	 */
        /* 			 */
        /******************** */

        function form() {
            // OBRAZAC
            echo '<form name="form1" method="post" action="?a=insert">
  	<fieldset><legend>Unos nove ankete</legend>&nbsp;</p>
  	<p>Pitanje: 
    	<input name="pitanje" type="text" id="pitanje" size="60">
  	</p>
  	<p>Odgovor 1: 
    	<input name="o1" type="text" id="o1">
  	</p>
  	<p>Odgovor 2: 
    	<input name="o2" type="text" id="o2">
  	</p>
  	<p>Odgovor 3: 
    	<input name="o3" type="text" id="o3">
  	</p>
  	<p>Odgovor 4: 
    	<input name="o4" type="text" id="o4">
  	</p>
  	<p>
    	<input type="submit" name="Submit" value="Submit">
  	</p>
  	</fieldset>
	</form>';
        }
        ?>

    </body>
</html>
