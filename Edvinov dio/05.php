﻿<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>

<?php
// 1. korak - dohvacanje trazene akcije (ili default vrijednost)
if(isset($_GET['akcija']))
{
	$a = $_GET['akcija'];
}
else
{
	$a = '';
}

// 2. korak - izvrsavanje trazene akcije ili default akcije
switch($a)
{
	case 'pf': display_login_form(); break;
	case 'rf': display_register_form(); break;
	case 'pp': login(); break;
	case 'rp': register(); break;
	default: display_login_form();
}

// 3. korak - sve trazene akcije (funkcije)
function display_login_form()
{
	echo '<form name="form1" method="post" action="02.php">
  <p>Prijava</p>
  <p>Korisničko ime: 
    <input type="text" name="username">
  </p>
  <p>Password: 
    <input type="password" name="password">
  </p>
  <p>
    <input name="prijava" type="submit" value="Prijava">
  </p>
</form>

<p><a href="?akcija=rf">Registriraj se!</a></p>';
}
function display_register_form()
{
	echo '<form name="form2" method="post" action="03.php">
  <p>Registracija</p>
  <p>Ime: 
    <input type="text" name="ime">
  </p>
  <p>Prezime: 
    <input type="text" name="prezime">
  </p>
  <p>Korisničko ime: 
    <input type="text" name="username">
  </p>
  <p>Password: 
    <input type="password" name="password">
  </p>
  <p>Ponovi password: 
    <input name="ppassword" type="password" id="ppassword">
  </p>
  <p>Email: 
    <input type="text" name="email">
  </p>
  <p>Spol: 
    <input type="radio" name="spol" value="M">
    M 
    <input type="radio" name="spol" value="Z">
    Ž 
  </p>
  <p>Predložak: 
    <select name="predlozak" id="predlozak">
      <option value="P">Plavi</option>
      <option value="Z">Zeleni</option>
      <option value="N">Narančasti</option>
    </select>
  </p>
  <p>Newsletter: 
    <input name="newsletter" type="checkbox" id="newsletter" value="1" checked>
  </p>
  <p>Poruka adminu: 
    <textarea name="poruka" id="poruka"></textarea>
  </p>
<input type="submit"  name="submit2" value="Registracija">
  <input name="skriveno" type="hidden" id="skriveno" value="1">
</form>';
}
function login()
{
	foreach($_POST as $key=>$value)
	{
		echo "Kljuc = $key ===> $value <br> ";
	}
}
function register()
{
	// PROVJERA IME, PREZIME, USERNAME, EMAIL
	$ok = true;
	$poruke = array();
	
	$ime = $_POST['ime'];
	if(strlen($ime)<2)
	{
		$ok = false;
		$poruke[] = 'Prekratko ime';
	}
	
	$username = $_POST['username'];
	if(strlen($username)<6)
	{
		$ok = false;
		$poruke[] = 'Prekratki username';
	}
	
	// USPOREDI PASS
	$pass1 = $_POST['password'];
	$pass2 = $_POST['ppassword'];
	if($pass1!==$pass2)
	{
		$ok = false;
		$poruke[] = 'Passwordi nisu jednaki';
	}
	
	// NEWSLETTER
	if(isset($_POST['newsletter']))
	{
		$poruke[] = 'Korisnik želi newsletter';
	}
	
	// EMAIL
	$email = $_POST['email'];
	if(strlen($email)<6)
	{
		$ok = false;
		$poruke[] = 'Email prekratak';
	}
	
	// SPOL
	$spol = $_POST['spol'];
	if($spol == 'M')
	{
		//echo 'Muski spol <br>';
		$poruke[] = 'Spol: muski';
	}
	elseif($spol == 'Z')
	{
		//echo 'Zenski spol <br>';
		$poruke[] = 'Spol: zenski';
	}
	else
	{
		$ok = false;
		$poruke[] = 'Nepoznat spol';
		//echo 'Nepoznati spol<br>';
		
	}
	
	// PREDLOZAK
	$predlozak = $_POST['predlozak'];
	//echo 'Predlozak: ';
	/*
	if($pred == 'P')
	{
		echo 'plavi';
	}
	if($pred == 'Z')
	{
		echo 'zeleni';
	}
	if($pred == 'N')
	{
		echo 'narančasti';
	}
	echo '<br>';
	*/
	switch($predlozak)
	{
		case 'P': $poruke[] = 'Pred.: Plavi'; break;
		case 'Z': $poruke[] = 'Pred.: Zeleni'; break;
		case 'N': $poruke[] = 'Pred.: Narančasti'; break;
		default: $ok = false; $poruke[] = 'Greska';
	}
	//echo '<br>';
	
	
	// PORUKA ADMINU
	$poruka = $_POST['poruka'];
	if(!empty($poruka))
	{
		$poruke[] = $poruka;
	}
	
	if($ok)
	{
		echo 'Hvala na registraciji.';
	}
	else
	{
		foreach($poruke as $p)
		{
			echo $p.'<br>';
		}
	}
}
?>
</body>
</html>