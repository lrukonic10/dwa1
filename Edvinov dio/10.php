﻿<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php

$path = '/home/ijugo/anketa.txt';

if(isset($_GET['a']))
{
	$a = $_GET['a'];
}
else
{
	$a = '';
}
switch($a)
{
	case 'prikaz': display_anketa(); break;
	case 'glasaj': display_voting_form(); break;
	case 'unesi': save_vote(); break;
	default: pregled_svih_anketa();
}

function display_anketa()
{
	global $path;
	$id = $_GET['id']; // KOJU ANKETU UZETI IZ DATOTEKE
	
	$fh = fopen($path, 'r');
				
	while(($red = fgets($fh, 4096)) !== false)
	{
		$redak = explode("\t", $red);
		if($redak[0]==$id) // NASLI SMO ANKETU
		{
			// ISPIS SVIH PODATAKA I OBRASACA ZA GLASANJE
			ispisAnkete($redak);
			// NASLI SMO ANKETU, PREKINI WHILE PETLJU
			break;
		}
	}
	
	fclose($fh);
}

function save_vote()
{
	global $path;
	$id = $_POST['id'];
	$odgovor = $_POST['anketa'];
	
	$fh = fopen($path, 'r');
	$sve = array();
	while(($red = fgets($fh, 4096)) !== false)
	{
		$redak = explode("\t", $red);
		if($redak[0]==$id) // NASLI SMO ANKETU
		{
			$novi = trim($redak[$odgovor+5])+1;
			$red = implode("\t", $redak);
		}
		$sve[] = $red;
	}
	fclose($fh);	
	
	// OTVORITI FILE
	// UZETI REDAK PO REDAK U PETLJI
	// JE LI TO TREZENI REDAK?
		// NIJE -> UBACI GA U ARRAY NOVOG SADRZAJA
		// JE -> UVECAJ BROJ GLASOVA -> UBACI GA U ARRAY
	// PREGAZI STARI FILE NOVIM ARRAY-EM
	
	$sadrzaj = implode("\n", $sve);
	$fh = fopen($path, 'w');
	flock($fh, LOCK_EX);
	fwrite($fh, $redak);
	flock($fh, LOCK_UN);
	fclose($fh);
}

function pregled_svih_anketa()
{
	global $path;
	
	// Postoji li datoteka?
	if(file_exists($path))
	{
		// Mogu li je čitati?
		if(is_readable($path))
		{
			if(filesize($path)!=0)
			{
				// CITANJE IZ TXT DATOTEKE
				$fh = fopen($path, 'r');
				
				while(($red = fgets($fh, 4096)) !== false)
				{
					$redak = explode("\t", $red);
					echo '<a href="?a=prikaz&id='.$redak[0].'">'.$redak[1].'</a><br>';
				}
				
				fclose($fh);
			}
			else
			{
				echo 'Necu se spajati kad je prazan';
			}
		}
		else
		{
			echo 'Ne mogu čitati datoteku';
		}
	}
	else
	{
		echo 'Datoteka ne postoji';
	}
}

function ispisAnkete($redak)
{
	echo '<form action="?a=unesi" method="post">';
	echo '<h1>'.$redak[1].'</h1>';
	echo '<p><input type="radio" name="anketa" value="1">'.$redak[2].' ---> '.$redak[6].'</p>';
	echo '<p><input type="radio" name="anketa" value="2">'.$redak[3].' ---> '.$redak[7].'</p>';
	echo '<p><input type="radio" name="anketa" value="3">'.$redak[4].' ---> '.$redak[8].'</p>';
	echo '<p><input type="radio" name="anketa" value="4">'.$redak[5].' ---> '.$redak[9].'</p>';
	echo '<p><input type="submit" value="Glasaj!"></p>';
	echo '<input type="hidden" name="id" value="'.$redak[0].'">';
	echo '</form>';
}

?>
</body>
</html>
