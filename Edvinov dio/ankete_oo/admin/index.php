<?php

require '../classes/Utils.php';
require '../classes/Anketa.php';
require '../classes/AnketeManager.php';

$am = new AnketeManager();
$page = '';

if (isset($_GET['a'])) { $a = $_GET['a']; } else { $a = ''; }
switch ($a) {
	case 'insert' : $am->insert(); break; // UNOS NOVE ANKETE
	case 'editform': $am->editForm(); break; // OBRAZAC ZA IZMJENU
	case 'editsave': $am->save(); break; // SPREMI IZMJENE
	case 'confirm' : $am->confirm(); break; // JESTE LI SIGURNI
	case 'delete' : $am->del(); break; // BRISANJE
	default:
		$page = 'pregled';
		$ankete = $am->getAll();
}

require '../template/index.phtml';

?>