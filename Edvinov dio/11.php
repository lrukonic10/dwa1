﻿<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php
// PRIKAZI SVE ANKETE I "DELETE" LINK PORED SVAKE
// PRIKAZI SVE PODATKE ODABREANE ANKETE I POTVRDA
// IZBRISI ANKETU

$path = '/home/emocibob/public_html/dwa1/baza.txt';

if(isset($_GET['a']))
{
	$a = $_GET['a'];
}
else
{
	$a = '';
}
switch($a)
{
	case 'prikaz': display_anketa(); break;
	case 'glasaj': display_voting_form(); break;
	case 'unesi': save_vote(); break;
	default: pregled_svih_anketa();
}

function display_anketa()
{
	global $path;
	$id = $_GET['id']; // KOJU ANKETU UZETI IZ DATOTEKE
	
	$fh = fopen($path, 'r');
				
	while(($red = fgets($fh, 4096)) !== false)
	{
		$redak = explode("\t", $red);
		if($redak[0]==$id) // NASLI SMO ANKETU
		{
			echo '<h1>'.$redak[1].'</h1>';
			echo '<h2>Jeste li sigurni da zelite izbrisati anketu</h2>';
			echo '<form method="post" action="?a=unesi">';
			echo '<input type="radio" name ="odg" value="1">DA
				  <input type="radio" name ="odg" value="0">NE';
			echo '<input type="submit" name="Submit" value="Potvrdi">';
			echo '<input type="hidden" name="id" value="'.$redak[0].'">';
			echo '</form>';
			// NASLI SMO ANKETU, PREKINI WHILE PETLJU
			break;
		}
	}
	
	fclose($fh);
}

function save_vote()
{
	global $path;
	$id = $_POST['id'];
	$odgovor = $_POST['odg'];
	// GLAVNI IF
		// AKO "NE" (tj. 0) ONDA NIŠTA NE BRIŠEMO
		// INAČE SVE OVO ISPOD
	if ($odgovor == 0)
	{
		echo '<h1>Nista nije izbrisano!</h1>';
		echo '<p><a href="'.$_SERVER['SCRIPT_NAME'].'">Povratak na pregled</p>';
	}
	else
	{
		$fh = fopen($path, 'r');
		$sve = array();
		while(($red = fgets($fh, 4096)) !== false)
		{
			$redak = explode("\t", trim($red));
			if($redak[0]!=$id) // SVI RETCI OSIM ONOG KOJEG BRISEMO
			{
				$red = implode("\t", $redak); // VRATI U STRING
				$sve[] = $red; // DODAJ STRING U POLJE ZA PRIJENOS
			}
		}
		fclose($fh);	
		
		$sadrzaj = implode("\n", $sve); // PRETVORI U VELIKI STRING
		$fh = fopen($path, 'w'); // OTVORI FILE ZA WRITE
		flock($fh, LOCK_EX);
		fwrite($fh, $sadrzaj); // PREGAZI STARI SADRZAJ
		//var_dump($sadrzaj);	//echo $sadrzaj;
		flock($fh, LOCK_UN);
		fclose($fh);
		
		echo '<h1>Anketa je izbrisana!</h1>';
		echo '<p><a href="'.$_SERVER['SCRIPT_NAME'].'">Povratak na pregled</p>';
	}
}

function pregled_svih_anketa()
{
	global $path;
	
	// Postoji li datoteka?
	if(file_exists($path))
	{
		// Mogu li je čitati?
		if(is_readable($path))
		{
			if(filesize($path)!=0)
			{
				// CITANJE IZ TXT DATOTEKE
				$fh = fopen($path, 'r');
				
				while(($red = fgets($fh, 4096)) !== false)
				{
					$redak = explode("\t", $red);
					//echo '<a href="?a=prikaz&id='.$redak[0].'">'.$redak[1].'</a><br>';
					echo $redak[1];
					  echo '<a href="?a=prikaz&id='.$redak[0].'"> DELETE </a><br>';
				}
				
				fclose($fh);
			}
			else
			{
				echo 'Necu se spajati kad je prazan';
			}
		}
		else
		{
			echo 'Ne mogu čitati datoteku';
		}
	}
	else
	{
		echo 'Datoteka ne postoji';
	}
}

function ispisAnkete($redak)
{
	echo '<form action="?a=unesi" method="post">';
	echo '<h1>'.$redak[1].'</h1>';
	echo '<p><input type="radio" name="anketa" value="1">'.$redak[2].' ---> '.$redak[6].'</p>';
	echo '<p><input type="radio" name="anketa" value="2">'.$redak[3].' ---> '.$redak[7].'</p>';
	echo '<p><input type="radio" name="anketa" value="3">'.$redak[4].' ---> '.$redak[8].'</p>';
	echo '<p><input type="radio" name="anketa" value="4">'.$redak[5].' ---> '.$redak[9].'</p>';
	echo '<p><input type="submit" value="Glasaj!"></p>';
	echo '<input type="hidden" name="id" value="'.$redak[0].'">';
	echo '</form>';
}

?>
</body>
</html>