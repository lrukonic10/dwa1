<?php
// ZAGLAVLJE
$xml = new DOMDocument("1.0");
// OSNOVNI ELEMENT
$root = $xml->createElement("data");
$xml->appendChild($root);
// DODAJ OSNOVNI ELEMENT U DATOTEKU
// STVARANJE NODE-OVA (STRKTURA PODATAKA)
// <id>1</id>
$id   = $xml->createElement("id");
$idText = $xml->createTextNode('1');
// <id>1</id>
$id->appendChild($idText);
// <title></title>
$title   = $xml->createElement("title");
$titleText = $xml->createTextNode('"PHP Undercover"');
// <title>PHP...
$title->appendChild($titleText);

// UBACIVANJE ID I TITLE U BOK ELEMENT
$book = $xml->createElement("book");
$book->appendChild($id);
$book->appendChild($title);

// DODAJ SVE U DATOTEKU
$root->appendChild($book);

// PISANJE DATOTEKE NA SERVERU
$xml->formatOutput = true;
echo "<xmp>". $xml->saveXML() ."</xmp>";
$xml->save("mybooks.xml") or die("Error");

?>
