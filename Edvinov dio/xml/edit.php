<?php
$file = "books.xml";
$fp = fopen($file, "rb") or die("cannot open file");
$str = fread($fp, filesize($file));


     
     $xml = new DOMDocument();
     $xml->formatOutput = true;
     $xml->preserveWhiteSpace = false;
     $xml->loadXML($str) or die("Error");
     
     // original
     echo "<xmp>OLD:\n". $xml->saveXML() ."</xmp>";
     
     // get document element
     $root   = $xml->documentElement; // <books>
     $fnode  = $root->firstChild; // <book>
     
     // DOHVATI PO REDNOM BROJU
	 // PROMJENITI U -> DOHVATI PO VRIJEDNOSTIMA ELEMENTA -- TODO
	 // $data->id?
     $ori    = $fnode->childNodes->item(0); //$ori    = $fnode->childNodes->item(0);
     
     $id     = $xml->createElement("id");
     $idText = $xml->createTextNode("3");
     $id->appendChild($idText);
     
     $title     = $xml->createElement("title");
     $titleText = $xml->createTextNode("PHP Framework");
     $title->appendChild($titleText);
     
     $author     = $xml->createElement("author");
     $authorText = $xml->createTextNode("Reza Christian");
     $author->appendChild($authorText);
     
     $book   = $xml->createElement("book");
     $book->appendChild($id);
     $book->appendChild($title);
     $book->appendChild($author);
     // ZAMJENA
     $fnode->replaceChild($book,$ori);
     // ISPIS DOM OBJEKT NA EKRAN 
     echo "<xmp>NEW:\n". $xml->saveXML() ."</xmp>";
	 // ZAPISI NA DISK
	 //$xml->save("mybooks.xml") or die("Error");
     ?>
     