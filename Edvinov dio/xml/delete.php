<?php
// OTVORI DATOTEKU I POKUPI SADRZAJ
$file = "books.xml";
$fp = fopen($file, "rb") or die("cannot open file");
$str = fread($fp, filesize($file));

// OTVORI DOM OBJEKT I POPUNI PODACIMA   
$xml = new DOMDocument();
$xml->formatOutput = true;
$xml->preserveWhiteSpace = false;
$xml->loadXML($str) or die("Error");

// ISPISI ORIGINALNU DATOTEKU PRIJE BRISANJA
echo "<xmp>OLD:\n". $xml->saveXML() ."</xmp>";

// DOHVATI ROOT ELEMENT I PRVI ZAPIS
$root   = $xml->documentElement;
$fnode  = $root->firstChild;

// KOJI ZAPIS TREBA IZBRISATI? redni br od 0
$ori    = $fnode->childNodes->item(1);

// IZBACI GA IZ DOM OBJEKTA U MEMORIJI
$fnode->removeChild($ori);
// ISPISI DOM OBJEKT U MEMORIJU
echo "<xmp>NEW:\n". $xml->saveXML() ."</xmp>";
// ZAPISI NA DISK
//$xml->save("mybooks.xml") or die("Error");
?>
