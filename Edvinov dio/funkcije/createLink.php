﻿<?php
/*
* FUNKCIJA createLink
* ULAZ: $path - putanja do ime datoteke
		$podaci - asocijativni niz sa podacima za querystring
		$tekst - tekst linka
* IZLAZ: HTML link
* OPIS: Generira html link
* IZRADIO: Edvin Močibob
* DATUM ZADNJE IZMJENE: 2013-12-04
  PRIMJER:
  echo createLink('admin/index.php', array('a'=>'new', b=>1), 'Edit'); // echo createLink('admin/index.php', array(), 'Edit');
*/
function createLink($path, $podaci, $tekst)
{
	
}
/*
Primjer:
<a href="index.php?a=new&b=1&c=101010&d=abc">Edit</a>
*/
?>