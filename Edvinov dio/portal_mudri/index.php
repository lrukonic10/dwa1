﻿<html>
<head>
<title>Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<table width="75%" border="1" cellspacing="0" cellpadding="10">
  <tr> 
    <td colspan="3">PORTAL</td>
  </tr>
  <tr valign="top"> 
    <td width="16%">Navigacija</td>
    <td width="57%">
        <h1>Demonstracija razvoja komponenata za potrebe web portala</h1>
        <p>Primjer: sadržaj web portala treba obogatiti većim brojem "blokova" 
        (često se nazivaju "portlets") koji daju nove funkcionalnosti portalu,
        ili dohvaćaju korisne podatke iz drugih izvora</p>
        <p>Preduvjeti: "blokovi" moraju biti izrađeni u jednoj datoteci koja 
        može sadržavati i više klasa (ovisno o složenosti). Pokretanje bloka 
        mora biti moguće izvesti u 3 linije koda (uključenje datoteke, instanciranje i
        pozivanje inicijalne metode display</p>
        <p>Blokovi moraju imati konstruktorsku i display metodu, te po potrebi 
        dodatne metode. Dodatne metode pozvati će se na sljedeći način:</p>
        <p>&lt;a href="?blok=newsletter&a=potvrdi"&gt;</p>
    </td>
    <td width="27%"> 
      <p>Blokovi</p>
      <p>Anketa</p>
	  <?php
	  //require('/putanja/do/Anketa.php');
	  //$a = new Anketa(4);
	  //$a->display();
	  ?>
      <p>Tečajna lista</p>
	  <?php
	  require('classes/Tecajna.php');
	  
	  if(isset($_GET['b']) && $_GET['b']=='tecajna' && $_GET['a']='opsirnije'){
              $sve = Tecajna::sve();
              $t = new Tecajna($sve);
              $t->display();
          }
          else{
              $t = new Tecajna('EUR,USD,CHF');
              $t->display();
          }
	  ?>
      <p>Prijava newsletter</p>
      <?php
	  require('classes/Newsletter.php');
	  $n = new Newsletter();
	  
          if(isset($_GET['b']) && $_GET['b']=='newsletter' && $_GET['a']='insert'){
              $n->insert();
          }
          else{
              $n->display();
          }
          
	  ?>
   
      <p>Dionice</p>
	  <?php
	  require('classes/Dionice.php');
	  $d = new Dionice();
	  $d->display();
	  ?>
      
      <p>Login/Register</p>
       <?php
	  require('classes/Login.php');
	  $l = new Login();
	  if(isset($_GET['b']) && $_GET['b']=='login' && $_GET['a']='login'){
              $l->login();
          }
          elseif(isset($_GET['b']) && $_GET['b']=='login' && $_GET['a']='register'){
              $l->register();
          }
          else{
              $l->display();
          }
	  ?>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      </td>
  </tr>
  <tr> 
    <td colspan="3">Footer portala</td>
  </tr>
</table>
</body>
</html>
