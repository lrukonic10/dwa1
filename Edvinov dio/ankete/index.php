﻿<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php
// GLASANJE ZA ANKETU
	// PRIKAZI POPIS ANKETA KAO LINKOVE
	// PRIKAZI ODABRANU ANKETU
	// GLASAJ
	
$path = './anketa.txt';

if(isset($_GET['a'])) { $a = $_GET['a']; } else  { $a = ''; }
switch($a)
{
	case 'prikaz': display_anketa(); break;
	case 'unesi': save_vote(); break;
	default: pregled_svih_anketa();
} 


function display_anketa(){
	
	global $path;
	$id = $_GET['id']; // KOJU ANKETU UZETI IZ DATOTEKE
	
	$fh = fopen($path,'r');
	while (($red = fgets($fh, 4096)) !== false) 
	{	
		$redak = explode("\t",$red);
		if($redak[0]==$id) // NASLI SMO TRAZENU ANKETU
		{
			// ISPIS SVIH PODATAKA I OBRASCA ZA GLASANJE
			ispisAnkete($redak);
			// NASLI SMO ANKETU, PREKINI WHILE PETLJU
			break;
		}
	}
	fclose($fh); 
	
}

function save_vote()
{
	global $path;
	$id = $_POST['id'];
	$odgovor = $_POST['anketa'];

	$fh = fopen($path,'r');	// OTVORI FILE ZA CITANJE
	$sve = array();			// POLJE ZA PRIJENOS SADRZAJA
	while (($red = fgets($fh, 4096)) !== false) 
	{	
		$redak = explode("\t",$red);
		if($redak[0]==$id) // NASLI SMO TRAZENU ANKETU
		{
			$stupac = $odgovor+5; // STUPAC KOJI TREBA UVECATI
			$redak[$stupac]= trim($redak[$stupac])+1; // UVECAJ+1
			$red = implode("\t",$redak); // VRATI U STRING
		}
		$sve[]=trim($red); 	// DODAJ STRING U POLJE ZA PRIJENOS
	}
	fclose($fh);
	
	$sadrzaj = implode("\n",$sve); // PRETVORI U VELIKI STRING
	
	$sadrzaj = $sadrzaj."\n";
	
	$fh = fopen($path,'w'); // OTVORI FILE ZA WRITE (PREGAZI)
	flock($fh,LOCK_EX);
	//var_dump($sadrzaj);
	fwrite($fh,$sadrzaj); // PREGAZI STARI SADRZAJ
	flock($fh,LOCK_UN);
	fclose($fh);
	
	echo '<h1>Vaš glas je unesen!</h1>';
	echo '<p><a href="?a=prikaz&id='.$id.'">Povratak na anketu</a></p>';
	echo  '<p><a href="./index.php">Povratak na pregled anketa</a></p>';
}



function pregled_svih_anketa()
{
	global $path;

	if(file_exists($path))// DA LI POSTOJI DATOTEKA?
	{
		if(is_readable($path))// MOGU LI JE JA ČITATI?
		{
			if(filesize($path)>0)// JE LI PRAZAN?
			{

				// CITANJE IZ TXT DATOTEKE
				$fh = fopen($path,'r');
				while (($red = fgets($fh, 4096)) !== false) 
				{	
					$redak = explode("\t",$red);
					echo '<a href="?a=prikaz&id='.$redak[0].'">'.$redak[1].'</a><br>';
				}
				fclose($fh); 
			} 
		}
	}
}

function ispisAnkete($redak){
	echo '<form action="?a=unesi" method="post">';
	echo '<h1>'.$redak[1].'</h1>';
	echo '<p><input type="radio" name="anketa" value="1">'.$redak[2].' ---> '.$redak[6].'</p>';
	echo '<p><input type="radio" name="anketa" value="2">'.$redak[3].' ---> '.$redak[7].'</p>';
	echo '<p><input type="radio" name="anketa" value="3">'.$redak[4].' ---> '.$redak[8].'</p>';
	echo '<p><input type="radio" name="anketa" value="4">'.$redak[5].' ---> '.$redak[9].'</p>';
	echo '<p><input type="submit" name="Submit" value="Glasaj!"></p>';
	echo '<input type="hidden" name="id" value="'.$redak[0].'">';
	echo '</form>';
}	
?>
</body>
</html>
