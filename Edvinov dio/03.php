﻿<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>

<?php
// PROVJERA IME, PREZIME, USERNAME, EMAIL
$ok = true;
$poruke = array();

$ime = $_POST['ime'];
if(strlen($ime)<2)
{
	$ok = false;
	$poruke[] = 'Prekratko ime';
}

$username = $_POST['username'];
if(strlen($username)<6)
{
	$ok = false;
	$poruke[] = 'Prekratki username';
}

// USPOREDI PASS
$pass1 = $_POST['password'];
$pass2 = $_POST['ppassword'];
if($pass1!==$pass2)
{
	$ok = false;
	$poruke[] = 'Passwordi nisu jednaki';
}

// NEWSLETTER
if(isset($_POST['newsletter']))
{
	$poruke[] = 'Korisnik želi newsletter';
}

// EMAIL
$email = $_POST['email'];
if(strlen($email)<6)
{
	$ok = false;
	$poruke[] = 'Email prekratak';
}

// SPOL
$spol = $_POST['spol'];
if($spol == 'M')
{
	//echo 'Muski spol <br>';
	$poruke[] = 'Spol: muski';
}
elseif($spol == 'Z')
{
	//echo 'Zenski spol <br>';
	$poruke[] = 'Spol: zenski';
}
else
{
	$ok = false;
	$poruke[] = 'Nepoznat spol';
	//echo 'Nepoznati spol<br>';
	
}

// PREDLOZAK
$predlozak = $_POST['predlozak'];
//echo 'Predlozak: ';
/*
if($pred == 'P')
{
	echo 'plavi';
}
if($pred == 'Z')
{
	echo 'zeleni';
}
if($pred == 'N')
{
	echo 'narančasti';
}
echo '<br>';
*/
switch($predlozak)
{
	case 'P': $poruke[] = 'Pred.: Plavi'; break;
	case 'Z': $poruke[] = 'Pred.: Zeleni'; break;
	case 'N': $poruke[] = 'Pred.: Narančasti'; break;
	default: $ok = false; $poruke[] = 'Greska';
}
//echo '<br>';


// PORUKA ADMINU
$poruka = $_POST['poruka'];
if(!empty($poruka))
{
	$poruke[] = $poruka;
}

if($ok)
{
	echo 'Hvala na registraciji.';
}
else
{
	foreach($poruke as $p)
	{
		echo $p.'<br>';
	}
}

?>

</body>
</html>
