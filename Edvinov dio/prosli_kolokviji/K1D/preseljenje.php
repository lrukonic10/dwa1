<?php

    function ispisNiza($a){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }

    $upisano = false;
    
    if (isset($_POST["zemlja"])){
        $obrazac[0] = $_POST["ime"];
        $obrazac[1] = $_POST["prezime"];
        $obrazac[2] = $_POST["email"];
        $obrazac[3] = $_POST["godina"];
        $obrazac[4] = $_POST["zemlja"];
        
        /*
         * „ime  \t   prezime   \t   email    \t godina  \t naziv_zemlje \n “.
         */
        
        $dat = fopen("emigracija.txt", "a");
        
        $zaUpis = implode("\t",$obrazac) . "\n";
        
        flock($dat, LOCK_EX);
        $upisano = fwrite($dat, $zaUpis);
        flock($dat, LOCK_UN);
        fclose($dat);
    }
    
    $zemlje = array();

    $dat = fopen("countries.txt", "r");
    while(!feof($dat)){
        array_push ($zemlje, explode("\t", trim(fgets($dat))));
    }
    fclose($dat);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Preseljenje</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php //ispisNiza($zemlje); ?>
        <form method="POST">
            <h1 align="center">Preseljenje</h1>
            <table align="center" border="0" cellspacing="5" cellpadding="5">
                <tbody>
                    <tr>
                        <td>Zemlja:</td>
                        <td>
                            <select name="zemlja">
                                <?php
                                    foreach($zemlje as $zemlja){
                                        echo "<option>" . $zemlja[1] . "</option>\n";
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Ime:</td>
                        <td><input type="text" name="ime" /></td>
                    </tr>
                    <tr>
                        <td>Prezime:</td>
                        <td><input type="text" name="prezime" /></td>
                    </tr>
                    <tr>
                        <td>E-mail:</td>
                        <td><input type="text" name="email" /></td>
                    </tr>
                    <tr>
                        <td>Godine:</td>
                        <td><input type="text" name="godina" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" value="Pošalji" /></td>
                    </tr>
                </tbody>
            </table>
        </form>
        
        <?php
        
        if ($upisano){
            echo "<h3 align=\"center\" style=\"color: #0F0;\">Obrazac uspješno poslan!</h3>";
            echo "<div align=\"center\">" . $zaUpis . "</div>";
        }
        
        ?>
        
    </body>
</html>