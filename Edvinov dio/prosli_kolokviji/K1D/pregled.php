<?php

    function ispisNiza($a){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }

    $emigranti = array();

    $dat = fopen("emigracija.txt", "r");
    while(!feof($dat)){
        array_push ($emigranti, explode("\t", trim(fgets($dat))));
    }
    array_pop($emigranti);
    fclose($dat);
        
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Pregled emigranata</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1 align="center">Pregled emigranata</h1>
        <table align="center" cellspacing="1" cellpadding="5" border="1">
            <tr>
                <th>Ime</th>
                <th>Prezime</th>
                <th>E-mail</th>
                <th>Godina</th>
                <th>Država</th>
            </tr>
            <?php
                foreach ($emigranti as $emo){
            ?>
            <tr>
                <td><?=$emo[0]?></td>
                <td><?=$emo[1]?></td>
                <td><?=$emo[2]?></td>
                <td><?=$emo[3]?></td>
                <td><?=$emo[4]?></td>
            </tr>
            <?php
                }
            ?>
        </table>
        <?php ispisNiza($emigranti);?>
    </body>
</html>