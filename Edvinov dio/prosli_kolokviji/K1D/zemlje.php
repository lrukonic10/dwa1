<?php

    function ispisNiza($a){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }

    $zemlje = array();

    $dat = fopen("countries.txt", "r");
    while(!feof($dat)){
        array_push ($zemlje, explode("\t", trim(fgets($dat))));
    }
    fclose($dat);
    
    
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Zemlje</title>
        <meta charset="UTF-8">
    </head>
    <body>
        
        <?php //ispisNiza($zemlje); ?>
        
        
        <table border = "1">
            <tr>
                <th>Rbr.</th>
                <th>CODE</th>
                <th>Ime</th>
                <th>Kontinent</th>
                <th>Regija</th>
                <th>Površina</th>
                <th>INDEP_YEAR</th>
                <th>Populacija</th>
                <th>Životni vijek</th>
                <th>GNP</th>
                <th>GNP_OLD</th>
                <th>Lokalno ime</th>
                <th>Government_form</th>
                <th>Head of state</th>
                <th>Capital</th>
                <th>CODE2</th>
            </tr>
            
            <?php
            
            for($i = 0; $i<15; $i++){ // $i = 15 je zadan u zadatku (prvih 15 redaka treba ispisati)
                echo "<tr>";
                echo "<td>".($i+1)."</td>";
                for($j = 0; $j<15; $j++){ // $j = 15 jer svaka država ima točno 15 podataka (stupaca)
                    echo "<td>" . $zemlje[$i][$j] . "</td>";
                }
                echo "</tr>";
            }
            
            /*
             * Zadaci 1. i 2. nisu dovršeni jer nije ispravno postavljeni!
             */
            
            ?>
            
            
            
        </table>
        
    </body>
</html>