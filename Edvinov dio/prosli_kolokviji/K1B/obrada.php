<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<?php

$cijena = 0;
$proizvod = $_POST['proizvod'];
$velicina = $_POST['velicina'];
$imeS = $_POST['id'];
$path = './narudzbe.txt';
$ok = false;

switch($proizvod)
{
	case 'T-shirt': $cijena += 100; break;
	case 'Trenerka': $cijena += 200; break;
	case 'Traperice': $cijena += 300; break;
	default: echo '<p>Greska</p>';	
}

switch($velicina)
{
	case 'LARGE': $cijena += 100; break;
	case 'EXTRALARGE': $cijena += 200; break;
	case 'XXL': $cijena += 300; break;
	default: echo '<p>Greska</p>';	
}

echo 'Ukupna cijena: '.$cijena.' kn<br>';

define('SEP', "\t");
$redak = $imeS.SEP.$proizvod.SEP.$velicina.SEP.$cijena.SEP.date('d-m-Y')."\n";

if (is_writeable($path)) $ok = true;

if ($ok)
{
	$fh = fopen($path, 'a');
	flock($fh, LOCK_EX);
	fwrite($fh, $redak);
	flock($fh, LOCK_UN);
	fclose($fh);
	echo '<p>Uspješan unos</p>';
}

if(file_exists($path))
{
	if(is_readable($path))
	{
		$fh = fopen($path, 'r');
		echo '<table border="1">';
		while(($red = fgets($fh, 4096)) !== false)
		{
			$redak = explode("\t", $red);
			echo '<tr>';
			foreach($redak as $r)
			{
				echo '<td>'.$r.'</td>';
			}
			echo '</tr>';
		}
		echo '</table>';
		fclose($fh);
	}
}

echo '<a href="./login.htm">Početak</p>';

?>

</body>
</html>